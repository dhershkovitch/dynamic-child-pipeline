$script1 = @"
stages:
    - build

job-1-1:
  stage: build
  script:
    - echo 'job 1 for job-1'

job-1-2:
  stage: build
  script:
    - echo 'job 2 for job-1'
"@;
Set-Content -Path job-1-config.yml -value $script1

$script2 = @"
stages:
    - build

job-2-1:
  stage: build
  script:
    - echo 'job 1 for job-2'

job-2-2:
  stage: build
  script:
    - echo 'job 2 for job-2'
"@;
Set-Content -Path job-2-config.yml -value $script2

$script3 = @"
stages:
    - build

job-3-1:
  stage: build
  script:
    - echo 'job 1 for job-3'

job-3-2:
  stage: build
  script:
    - echo 'job 2 for job-3'
"@;
Set-Content -Path job-3-config.yml -value $script3
